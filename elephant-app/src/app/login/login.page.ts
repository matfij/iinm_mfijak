import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesService } from '../@services/utilities.service';
import { ValidatorService } from '../@services/validator.service';
import { ApiService } from '../@services/api.service';
import { LoginData, LoginResponse } from '../@data/models';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html'
})
export class LoginPage implements OnInit {

  public _login: string;
  public _password: string;

  constructor(
    private router: Router,
    private utilities: UtilitiesService,
    private validator: ValidatorService,
    private api: ApiService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {}

  login() {
    if (this.validator.validateString(this._login)) {
      this.utilities.presentToast('You must enter login');
      return;
    }
    else if (this.validator.validateString(this._password)) {
      this.utilities.presentToast('You must enter password');
      return;
    }

    const loginData: LoginData = {
      login: this._login,
      password: this._password
    }

    this.api.login(loginData).subscribe((response: LoginResponse) => {
      if (response.user) {
        this.utilities.login(response.user);
        this.router.navigate(['/home']);
      }
      this.utilities.presentToast(response.message);
    }, error => {
      this.utilities.presentToast('Server error: ' + error);
    });
  }

}
