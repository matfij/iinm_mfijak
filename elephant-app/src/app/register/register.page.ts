import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidatorService } from '../@services/validator.service';
import { UtilitiesService } from '../@services/utilities.service';
import { User, MessageResponse } from '../@data/models';
import { ApiService } from '../@services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html'
})
export class RegisterPage implements OnInit {

  public _userForm = new FormGroup({
    login: new FormControl('',),
    email: new FormControl(''),
    password: new FormControl(''),
    confirmPassword: new FormControl('')
  });
  public _invalidFields = {};

  constructor(
    private api: ApiService,
    private utilities: UtilitiesService,
    private validator: ValidatorService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {}

  onSubmit() {
    const login = this._userForm.value['login'];
    const email = this._userForm.value['email'];
    const password = this._userForm.value['password'];
    const confirmPassword = this._userForm.value['confirmPassword'];

    this._invalidFields = {};

    if (this.validator.validateString(login))  { this._invalidFields["login"] = true; }
    if (this.validator.validateEmail(email))  { this._invalidFields["email"] = true; }
    if (this.validator.validateString(password))  { this._invalidFields["password"] = true; }
    if (this.validator.validateString(confirmPassword))  { this._invalidFields["confirmPassword"] = true; }

    var valid = true;
    Object.keys(this._invalidFields).forEach(key => { if (this._invalidFields[key] === true)  valid = false; });
    if (!valid) {
      this.utilities.presentToast('You must enter all required fiels');
      return;
    }

    if (this.validator.validatePassword(password, confirmPassword)) {
      this._invalidFields["password"] = true;
      this._invalidFields["confirmPassword"] = true;
      this.utilities.presentToast('Password must be at least 8 character long, and match Confrim password field', 3000);
      return;
    }

    const newUser: User = {
      login: login,
      email: email,
      password: password
    }

    this.api.createUser(newUser).subscribe((response: MessageResponse) => {
      if (response.success) {
        this.router.navigate(['/login']);
      }
      this.utilities.presentToast(response.message);
    }, error => {
      this.utilities.presentToast('Server error: ' + error);
    });
  }

}
