export interface User {
    id?: number;
    login: string;
    password: string;
    email: string;
}

export interface LoginData {
    login: string;
    password: string;
}

export interface LoginResponse {
    user?: User;
    message: string;
}

export interface MessageResponse {
    message: string;
    success: boolean;
}

export interface Item {
    id?: number;
    name: string;
    price: number;
    image: string;
}

export interface Purchase {
    id?: number;
    userId: number;
    itemId: number;
    date?: string;
    quantity: number;
}

export interface CartItem {
    id: number;
    name: string;
    quantity: number;
    image: string;
    totalPrice: number;
}

export interface HistoryEntry {
    itemName: string;
    date: string;
    itemQuantity: number;
    totalPrice: number;
}