import { Component, OnInit } from '@angular/core';
import { Purchase, Item, CartItem } from '../@data/models';
import { UtilitiesService } from '../@services/utilities.service';
import { ApiService } from '../@services/api.service';
import { ConstantsService } from '../@services/constants.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ValidatorService } from '../@services/validator.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  public _displayedItems: CartItem[] = [];
  public _purchases: Purchase[] = [];
  public _totalPrice: number = 0;

  constructor(
    private utilities: UtilitiesService,
    private api: ApiService,
    private constants: ConstantsService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    if (!this.utilities.loggedIn) {
      this.router.navigate(['/login']);
      return;
    }
    else {
      this._purchases = this.utilities.cart;
      this.api.getAllItems().subscribe((items: Item[]) => {

        if (items.length > this.constants.ZERO) {
          items.forEach((item: Item) => {
            
            this._purchases.forEach((purchase: Purchase) => {
              if (item.id === purchase.itemId) {
                this._displayedItems.push({
                  id: item.id,
                  name: item.name,
                  image: item.image,
                  quantity: purchase.quantity,
                  totalPrice: (item.price * purchase.quantity)
                });
                this._totalPrice += (item.price * purchase.quantity);
              }
            });
          });
        }
        else {
          this.utilities.presentToast('Inventory is empty');
        }
      }, error => {
        this.utilities.presentToast('Server error - could not load inventory');
      });
    }
  }

  remove(itemId: number, itemName: string) {
    const purchaseInd = this._purchases.findIndex(i => i.itemId === itemId);
    const displayedItemsInd = this._displayedItems.findIndex(i => i.id === itemId);
    const cartItemsInd = this.utilities.cart.findIndex(i => i.itemId === itemId);

    this._totalPrice -= this._displayedItems[displayedItemsInd].totalPrice; 

    this._purchases.splice(purchaseInd, 1);
    this._displayedItems.splice(displayedItemsInd, 1);
    this.utilities.cart.splice(cartItemsInd, 1);

    this.utilities.presentToast(itemName + ' was removed from cart')
  }

  purchase() {
    this._purchases.forEach((purchase: Purchase) => {
      this.api.createPurchase(purchase).subscribe();
    });
    
    this._totalPrice = 0;
    this._displayedItems = [];
    this._purchases = [];
    this.utilities.cart = [];

    this.utilities.presentToast('Purchase completed');
    this.router.navigate(['/home'])
  }

}
