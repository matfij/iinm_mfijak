import { Component, OnInit } from '@angular/core';
import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ConstantsService } from './@services/constants.service';
import { UtilitiesService } from './@services/utilities.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Inventory',
      url: '/inventory',
      icon: 'cube'
    },
    {
      title: 'Shopping cart',
      url: '/cart',
      icon: 'cart'
    },
    {
      title: 'Purchases history',
      url: '/history',
      icon: 'time'
    }
  ];

  public _userName: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private constants: ConstantsService,
    private utilities: UtilitiesService,
    private router: Router,
    public menuCtrl: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  get appVersion() { return this.constants.APP_VERSION; }

  ngOnInit() {
    this.utilities.userName.subscribe(userName => {
      this._userName = userName;
    });
  }

  logout() {
    this.menuCtrl.toggle();
    this.utilities.logout();
    this.router.navigate(['/login']);
  }

}
