import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../@services/utilities.service';
import { ApiService } from '../@services/api.service';
import { Router } from '@angular/router';
import { Item, Purchase } from '../@data/models';
import { ConstantsService } from '../@services/constants.service';
import { AlertController } from '@ionic/angular';
import { ValidatorService } from '../@services/validator.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.page.html',
  styleUrls: ['./inventory.page.scss']
})
export class InventoryPage implements OnInit {

  public _items: Item[] = [];

  constructor(
    private utilities: UtilitiesService,
    private api: ApiService,
    private constants: ConstantsService,
    private router: Router,
    private alertController: AlertController,
    private validator: ValidatorService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    if (!this.utilities.loggedIn) {
      this.router.navigate(['/login']);
      return;
    }
    else {
      this.api.getAllItems().subscribe((items: Item[]) => {
        if (items.length > this.constants.ZERO) {
          this._items = items;
        }
        else {
          this.utilities.presentToast('Inventory is empty');
        }
      }, error => {
        this.utilities.presentToast('Server error - could not load inventory');
      });
    }
  }

  async chooseQuantity(itemId: number, itemName: string) {
    const alert = await this.alertController.create({
      header: 'Enter quantity',
      inputs: [
        {
          name: 'quantity',
          type: 'number',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {}
        }, {
          text: 'Add',
          handler: (data) => {
            this.addToCart(itemId, itemName, data['quantity'])
          }
        }
      ]
    });

    await alert.present();
  }

  addToCart(itemId: number, itemName: string, quantity: number) {
    if (this.validator.validateQuantity(quantity)) {
      this.utilities.presentToast('Quantity must be in range 1 ... 100');
      return;
    }
    var newPurchase: Purchase = {
      userId: this.utilities.user.id,
      itemId: itemId,
      quantity: +quantity
    };

    const ind = this.utilities.cart.findIndex(i => i.itemId === itemId);

    if (ind === -this.constants.ONE) {
      this.utilities.cart.push(newPurchase);
    } else {
      newPurchase.quantity += this.utilities.cart[ind].quantity;
      this.utilities.cart[ind] = newPurchase;
    }

    this.utilities.presentToast(itemName + ' added to cart')
  }

}
