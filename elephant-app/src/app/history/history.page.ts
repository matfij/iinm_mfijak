import { Component, OnInit } from '@angular/core';
import { Purchase, Item, HistoryEntry } from '../@data/models';
import { UtilitiesService } from '../@services/utilities.service';
import { ApiService } from '../@services/api.service';
import { ConstantsService } from '../@services/constants.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  public _purchases: Purchase[] = [];
  public _historyEntries: HistoryEntry[] = [];

  constructor(
    private utilities: UtilitiesService,
    private api: ApiService,
    private constants: ConstantsService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    if (!this.utilities.loggedIn) {
      this.router.navigate(['/login']);
      return;
    }
    else {
      const userId = this.utilities.user.id;

      this.api.getAllItems().subscribe((items: Item[]) => {

        this.api.getPurchaseByUserId(userId).subscribe((purchases: Purchase[]) => {

          if (purchases.length > this.constants.ZERO) {
            this._purchases = purchases;
            this._purchases.forEach((purchase: Purchase) => {
              const ind = items.findIndex(i => i.id === purchase.itemId);
              const refItem = items[ind];
              this._historyEntries.push({
                date: purchase.date,
                itemName: refItem.name,
                itemQuantity: purchase.quantity,
                totalPrice: Math.round((purchase.quantity * refItem.price) * 100) / 100
              });
            });
          }
        }, error => {
          this.utilities.presentToast('Server error - could not load purchases history');
        });
      }, error => {
        this.utilities.presentToast('Server error - could not load purchases history');
      });
      
    }
  }

}
