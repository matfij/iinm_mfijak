import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesService } from '../@services/utilities.service';
import { ApiService } from '../@services/api.service';
import { Item, Purchase } from '../@data/models';
import { ConstantsService } from '../@services/constants.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  public _items: Item[];
  public _latestItem: Item;
  public _latestItemDescription: Item;

  constructor(
    private router: Router,
    private utilities: UtilitiesService,
    private api: ApiService,
    private constants: ConstantsService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    if (!this.utilities.loggedIn) {
      this.router.navigate(['/login']);
      return;
    }
    else {
      this.api.getAllItems().subscribe((items: Item[]) => {
        if(items.length > this.constants.ZERO) {
          this._items = items;
          this._latestItem = this._items[items.length-1];
        }
        else {
          this.utilities.presentToast('Inventory is empty');
        }
      }, error => {
        this.utilities.presentToast('Server error - could not load inventory');
      });
    }
  }

  addToCart() {
    const newPurchase: Purchase = {
      userId: this.utilities.user.id,
      itemId: this._latestItem.id,
      quantity: this.constants.ONE
    };
    this.utilities.cart.push(newPurchase);
    this.utilities.presentToast(this._latestItem.name + ' added to cart')
  }

}
