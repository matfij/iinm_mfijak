import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor(
    private constants: ConstantsService
  ) {}

  validateString(input: string): boolean {
    return !(input && input.length > this.constants.MIN_STRING_LEN && input.length < this.constants.MAX_STRING_LEN);
  }

  validateInt(input: number) {
    return !(input || input === this.constants.ZERO);
  }

  validateQuantity(input: number) {
    return !(input > this.constants.MIN_QUANTITY && input < this.constants.MAX_QUANTITY);
  }

  validatePassword(password: string, confirmPassword: string): boolean {
    return !(password.length > this.constants.MIN_PASSWORD_LEN && password === confirmPassword);
  }

  validateEmail(input: string): boolean {
    return !(input && input.length > this.constants.MIN_EMAIL_LEN && input.includes('@') && input.includes('.'));
  }
}
