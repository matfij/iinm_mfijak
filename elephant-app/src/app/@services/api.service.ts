import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginData, LoginResponse, User, MessageResponse, Item, Purchase } from '../@data/models';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _BASE_URL = 'http://localhost:8080/';

  constructor(private http: HttpClient) {}

  /*
    User
  */
  login(loginData: LoginData): Observable<LoginResponse> {
    const targetUrl = this._BASE_URL + 'users/login';
    return this.http.post<LoginResponse>(targetUrl, loginData);
  }

  createUser(user: User): Observable<MessageResponse> {
    const targetUrl = this._BASE_URL + 'users/create';
    return this.http.post<MessageResponse>(targetUrl, user);
  }

  /*
    Items
  */
  getAllItems(): Observable<Item[]> {
    const targetUrl = this._BASE_URL + 'items/get';
    return this.http.get<Item[]>(targetUrl);
  }

  /*
    Purchases
  */
  getPurchaseByUserId(id: number): Observable<Purchase[]> {
    const targetUrl = this._BASE_URL + 'purchases/getByUser/' + id;
    return this.http.get<Purchase[]>(targetUrl);
  }

  createPurchase(purchase: Purchase): Observable<MessageResponse> {
    const targetUrl = this._BASE_URL + 'purchases/create';
    return this.http.post<MessageResponse>(targetUrl, purchase);
  }

}