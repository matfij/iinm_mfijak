import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {

  constructor() {}

  // general
  public readonly APP_VERSION = 'ver 0.1.0';
  public readonly DATE_FORMAT = 'yyyy.MM.dd';
  public readonly ZERO = 0;
  public readonly ONE = 1;

  // validation
  public readonly MAX_STRING_LEN = 128;
  public readonly MIN_STRING_LEN = 0;
  public readonly MIN_PASSWORD_LEN = 7;
  public readonly MIN_EMAIL_LEN = 5;
  public readonly MIN_QUANTITY = 0;
  public readonly MAX_QUANTITY = 99;

}
