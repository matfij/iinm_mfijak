import { Injectable, Output, EventEmitter } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { User, Item, Purchase } from '../@data/models';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  public user: User;
  @Output() public userName = new EventEmitter<string>();
  public loggedIn: boolean = false;
  public cart: Purchase[] = [];

  constructor(
    private toastController: ToastController
  ) {}

  async presentToast(message: string, duration: number = 2000) {
    const toast = await this.toastController.create({
      message: message,
      duration: duration
    });
    toast.present();
  }

  login(user: User) {
    this.user = user;
    this.loggedIn = true;
    this.userName.emit(this.user.login);
  }

  logout() {
    this.user = null;
    this.loggedIn = false;
    this.cart = [];
  }

}
