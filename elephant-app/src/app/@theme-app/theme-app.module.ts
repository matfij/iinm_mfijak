import { IonicModule } from '@ionic/angular';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    ReactiveFormsModule,
    DatePipe,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    DatePipe
  ]
})
export class ThemeAppModule {
  public static forChild(): ModuleWithProviders {
    return {
      ngModule: ThemeAppModule
    };
  }
}
