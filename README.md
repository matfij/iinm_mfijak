# Elephant

Electronics shop project for Informatics in Mechatronics AGH course.

## API Requirements

- Java JDK 1.8+
- Java Servlet/Tomcat

## PWA Requirements

- Node JS
- Angular CLI
- Yarn

## Mobile Requirements
- Android SDK
- Ionic CLI
- Yarn
- Capacitor

## Running project
- start Apache service
- start MySQL service
- start Tomcat service
- build and run api project (./elephant-api/target/classes)
- build run apps (ionic build, then ionic s for PWA, ionic cap run android for Android, ionic cap run ios for IOS)

## License
[MIT](https://choosealicense.com/licenses/mit/)