package com.agh.fijak.api.models;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RequestParam;

@JsonPOJOBuilder
public class UserModel {

    public UserModel() {}

    public UserModel(
            @RequestParam(required = false) @Nullable int id,
            String login,
            String password,
            String email
        ) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public int id;
    public String login;
    public String password;
    public String email;
}
