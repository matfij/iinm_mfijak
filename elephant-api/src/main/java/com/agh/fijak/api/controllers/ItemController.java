package com.agh.fijak.api.controllers;

import com.agh.fijak.api.models.ItemModel;
import com.agh.fijak.api.models.MessageResponse;
import com.agh.fijak.api.services.UtilitiesService;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public final class ItemController extends BaseController {

    private static final String TABLE_NAME = "items";
    private static final String ENDPOINT = "/items/";

    @RequestMapping(value = ENDPOINT + "create", method = RequestMethod.POST)
    public MessageResponse create(@RequestBody ItemModel newItem) throws Exception {
        MessageResponse messageResponse = new MessageResponse();

        this.initializeConnection();

        if (!UtilitiesService.validateString(newItem.name)) {
            messageResponse.message = "Item name invalid";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validatePrice(newItem.price)) {
            messageResponse.message = "Item price invalid";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validateImage(newItem.image)) {
            messageResponse.message = "Item image invalid";
            messageResponse.success = false;
        }
        else {
            String query = "insert into " + TABLE_NAME + " (name, price, image) values (" +
                    "\"" + newItem.name + "\", " +
                    "" + newItem.price + ", " +
                    "\"" + newItem.image + "\")";
            stmt.executeUpdate(query);
            con.close();

            messageResponse.message = "New item created";
            messageResponse.success = true;
        }

        return messageResponse;
    }

    @RequestMapping(value = ENDPOINT + "get", method = RequestMethod.GET)
    public List<ItemModel> get() throws Exception {
        List<ItemModel> items = new ArrayList<>();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME;
        ResultSet response = stmt.executeQuery(query);

        while (response.next())
            items.add( new ItemModel(
                    response.getInt(1),
                    response.getString(2),
                    response.getDouble(3),
                    response.getString(4)
            ));
        con.close();

        return items;
    }

    @RequestMapping(value = ENDPOINT + "get/{id}", method = RequestMethod.GET)
    public ItemModel getById(@PathVariable int id) throws Exception {
        ItemModel item = new ItemModel();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME + " where id = " + id;
        ResultSet response = stmt.executeQuery(query);

        while (response.next()) {
            item.id = response.getInt(1);
            item.name = response.getString(2);
            item.price = response.getDouble(3);
            item.image = response.getString(4);
        }
        con.close();

        return item;
    }

    @RequestMapping(value = ENDPOINT + "update", method = RequestMethod.PUT)
    public MessageResponse update(@RequestBody ItemModel updatedItem) throws Exception {
        MessageResponse messageResponse = new MessageResponse();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME;
        ResultSet response = stmt.executeQuery(query);

        List<ItemModel> items = new ArrayList<>();
        while (response.next())
            items.add( new ItemModel(
                    response.getInt(1),
                    response.getString(2),
                    response.getDouble(3),
                    response.getString(4)
            ));

        Optional<ItemModel> optionalItem = items.stream()
                .filter(i -> i.id == updatedItem.id).findFirst();

        ItemModel refItem = optionalItem.orElse(null);

        if (refItem == null) {
            messageResponse.message = "Item not found";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validateString(updatedItem.name)) {
            messageResponse.message = "Item name invalid";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validatePrice(updatedItem.price)) {
            messageResponse.message = "Item price invalid";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validateImage(updatedItem.image)) {
            messageResponse.message = "Item image invalid";
            messageResponse.success = false;
        }
        else {
            query = "update " + TABLE_NAME + " set " +
                    "name = \"" + updatedItem.name + "\", " +
                    "price = " + updatedItem.price + ", " +
                    "image = \"" + updatedItem.image + "\"" +
                    "where id = " + updatedItem.id;
            stmt.executeUpdate(query);

            messageResponse.message = "Item of id: " + updatedItem.id + " was updated";
            messageResponse.success = true;
        }
        con.close();

        return messageResponse;
    }

    @RequestMapping(value = ENDPOINT + "delete/{id}", method = RequestMethod.DELETE)
    public MessageResponse delete(@PathVariable int id) throws Exception {
        MessageResponse messageResponse = new MessageResponse();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME;
        ResultSet response = stmt.executeQuery(query);

        List<ItemModel> items = new ArrayList<>();
        while (response.next())
            items.add( new ItemModel(
                    response.getInt(1),
                    response.getString(2),
                    response.getDouble(3),
                    response.getString(4)
            ));

        Optional<ItemModel> optionalItem = items.stream()
                .filter(i -> i.id == id).findFirst();

        ItemModel refItem = optionalItem.orElse(null);

        if (refItem == null) {
            messageResponse.message = "Item not found";
            messageResponse.success = false;
        }
        else {
            query = "delete from " + TABLE_NAME + " where id = " + id;
            stmt.executeUpdate(query);

            messageResponse.message = "Item of id: " + id + " was deleted";
            messageResponse.success = true;
        }
        con.close();

        return messageResponse;
    }

}
