package com.agh.fijak.api.controllers;

import com.agh.fijak.api.models.ItemModel;
import com.agh.fijak.api.models.MessageResponse;
import com.agh.fijak.api.models.PurchaseModel;
import com.agh.fijak.api.models.UserModel;
import com.agh.fijak.api.services.UtilitiesService;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public final class PurchaseController extends BaseController {

    private static final String TABLE_NAME = "purchases";
    private static final String ENDPOINT = "/purchases/";

    @RequestMapping(value = ENDPOINT + "getByUser/{id}", method = RequestMethod.GET)
    public List<PurchaseModel> getByUserId(@PathVariable int id) throws Exception {
        List<PurchaseModel> purchases = new ArrayList<>();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME + " where userId = " + id;
        ResultSet response = stmt.executeQuery(query);

        while (response.next())
            purchases.add( new PurchaseModel(
                    response.getInt(1),
                    response.getInt(2),
                    response.getInt(3),
                    response.getString(4),
                    response.getInt(5)
            ));

        con.close();

        return purchases;
    }

    @RequestMapping(value = ENDPOINT + "create", method = RequestMethod.POST)
    public MessageResponse create(@RequestBody PurchaseModel newPurchase) throws Exception {
        MessageResponse messageResponse = new MessageResponse();

        this.initializeConnection();

        String query = "select * from users";
        ResultSet response = stmt.executeQuery(query);

        List<UserModel> users = new ArrayList<>();
        while (response.next()) {
            users.add( new UserModel(
                    response.getInt(1),
                    response.getString(2),
                    response.getString(3),
                    response.getString(4)
            ));
        }

        Optional<UserModel> optionalUser = users.stream()
                .filter(u -> u.id == newPurchase.userId).findFirst();

        UserModel refUser = optionalUser.orElse(null);

        query = "select * from items";
        response = stmt.executeQuery(query);

        List<ItemModel> items = new ArrayList<>();
        while (response.next())
            items.add( new ItemModel(
                    response.getInt(1),
                    response.getString(2),
                    response.getDouble(3),
                    response.getString(4)
            ));

        Optional<ItemModel> optionalItem = items.stream()
                .filter(i -> i.id == newPurchase.itemId).findFirst();

        ItemModel refItem = optionalItem.orElse(null);

        if (refUser == null) {
            messageResponse.message = "User not found";
            messageResponse.success = false;
        }
        else if (refItem == null) {
            messageResponse.message = "Item not found";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validateQuantity(newPurchase.quantity)) {
            messageResponse.message = "Invalid item quantity";
            messageResponse.success = false;
        }
        else {
            query = "insert into " + TABLE_NAME + " (userId, itemId, date, quantity) values (" +
                    newPurchase.userId + ", " +
                    newPurchase.itemId + ", " +
                    "\"" + UtilitiesService.getDate() + "\"" + ", " +
                    newPurchase.quantity + ")";

            stmt.executeUpdate(query);

            messageResponse.message = "Purchase added";
            messageResponse.success = true;
        }

        con.close();

        return messageResponse;
    }
}
