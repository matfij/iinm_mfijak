package com.agh.fijak.api.services;

public final class ConstantsService {

    // DB data
    public static final String DB_URL = "jdbc:mysql://localhost:3306/elephant";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "";

    // security
    public static final String ALLOWED_ORIGIN = "http://localhost:8100";

    // misc
    public static final int MAX_STRING_LEN = 127;
    public static final int MIN_STRING_LEN = 1;
    public static final int MIN_IMAGE_LEN = 10;
    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DATE_FORMAT = "yyyy-MM-dd 'at' HH:mm:ss z";

    // user
    public static final int MIN_LOGIN_LEN = 3;
    public static final int MAX_LOGIN_LEN = 20;
    public static final int MIN_PASSWORD_LEN = 7;

    // items
    public static final double MAX_PRICE = 999999;
    public static final double MIN_PRICE = 0;

    // purchases
    public static final int MAX_QUANTITY = 99;
    public static final int MIN_QUANTITY = 0;
}
