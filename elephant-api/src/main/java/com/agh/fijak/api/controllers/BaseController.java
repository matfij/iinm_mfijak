package com.agh.fijak.api.controllers;

import com.agh.fijak.api.services.ConstantsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@CrossOrigin(origins = ConstantsService.ALLOWED_ORIGIN, maxAge = 3600)
@RestController
public class BaseController {

    String url = ConstantsService.DB_URL;
    String user = ConstantsService.DB_USER;
    String password = ConstantsService.DB_PASSWORD;
    Statement stmt;
    Connection con;

    protected void initializeConnection() throws ClassNotFoundException, SQLException {
        Class.forName(ConstantsService.DB_DRIVER);
        this.con = DriverManager.getConnection(url, user, password);
        this.stmt = con.createStatement();
    }
}
