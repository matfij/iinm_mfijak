package com.agh.fijak.api.controllers;

import com.agh.fijak.api.models.LoginData;
import com.agh.fijak.api.models.LoginResponse;
import com.agh.fijak.api.models.MessageResponse;
import com.agh.fijak.api.models.UserModel;
import com.agh.fijak.api.services.UtilitiesService;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public final class UserController extends BaseController {

    private static final String TABLE_NAME = "users";
    private static final String ENDPOINT = "/users/";

    @RequestMapping(value = ENDPOINT + "get", method = RequestMethod.GET)
    public List<UserModel> get() throws Exception {
        List<UserModel> users = new ArrayList<>();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME;
        ResultSet response = stmt.executeQuery(query);

        while (response.next())
            users.add( new UserModel(
                response.getInt(1),
                response.getString(2),
                response.getString(3),
                response.getString(4)
            ));
        con.close();

        return users;
    }

    @RequestMapping(value = ENDPOINT + "get/{id}", method = RequestMethod.GET)
    public UserModel getById(@PathVariable int id) throws Exception {
        UserModel user = new UserModel();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME + " where id = " + id;
        ResultSet response = stmt.executeQuery(query);

        while (response.next()) {
            user.id = response.getInt(1);
            user.login = response.getString(2);
            user.password = response.getString(3);
            user.email = response.getString(4);
        }
        con.close();

        return user;
    }

    @RequestMapping(value = ENDPOINT + "create", method = RequestMethod.POST)
    public MessageResponse create(@RequestBody UserModel newUser) throws Exception {
        MessageResponse messageResponse = new MessageResponse();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME;
        ResultSet response = stmt.executeQuery(query);

        List<UserModel> users = new ArrayList<>();
        while (response.next()) {
            users.add( new UserModel(
                    response.getInt(1),
                    response.getString(2),
                    response.getString(3),
                    response.getString(4)
            ));
        }

        Optional<UserModel> optionalUser = users.stream()
                .filter(u -> u.login.equals(newUser.login) || u.email.equals(newUser.email)).findFirst();

        UserModel refUser = optionalUser.orElse(null);

        if (refUser != null) {
            messageResponse.message = "Login/Email already occupied";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validateLogin(newUser.login)) {
            messageResponse.message = "Login must contain between 4 and 20 characters";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validateEmail(newUser.email)) {
            messageResponse.message = "Email must be valid";
            messageResponse.success = false;
        }
        else if (!UtilitiesService.validatePassword(newUser.password)) {
            messageResponse.message = "Password must contain between 8 and 128 characters";
            messageResponse.success = false;
        }
        else {
            String hashedPassword = UtilitiesService.hashString(newUser.password);

            query = "insert into " + TABLE_NAME + " (login, password, email) values (" +
                "\"" + newUser.login + "\", " +
                "\"" + hashedPassword + "\", " +
                "\"" + newUser.email + "\")";

            stmt.executeUpdate(query);

            messageResponse.message = "Account created";
            messageResponse.success = true;
        }
        con.close();

        return messageResponse;
    }

    @RequestMapping(value = ENDPOINT + "login", method = RequestMethod.POST)
    public LoginResponse login(@RequestBody LoginData loginData) throws Exception {
        LoginResponse loginResponse = new LoginResponse();

        this.initializeConnection();

        String query = "select * from " + TABLE_NAME;
        ResultSet response = stmt.executeQuery(query);

        List<UserModel> users = new ArrayList<>();
        while (response.next()) {
            users.add( new UserModel(
                    response.getInt(1),
                    response.getString(2),
                    response.getString(3),
                    response.getString(4)
            ));
        }
        con.close();

        Optional<UserModel> optionalUser = users.stream().filter(u -> u.login.equals(loginData.login)).findFirst();
        UserModel refUser = optionalUser.orElse(null);
        if (refUser == null) {
            loginResponse.message = "User not found";
        }
        else {
            if (UtilitiesService.compareWithHashedString(loginData.password, refUser.password)) {
                loginResponse.user = refUser;
                loginResponse.message = "Logged in successfully";
            }
            else {
                loginResponse.message = "Password incorrect";
            }
        }
        return loginResponse;
    }
}
