package com.agh.fijak.api.services;

import org.springframework.stereotype.Service;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import static ch.qos.logback.core.encoder.ByteArrayUtil.hexStringToByteArray;

@Service("utils")
public final class UtilitiesService {

    // hashing parameters
    private static final String HASH_METHOD = "PBKDF2WithHmacSHA1";
    private static final int I = 65536;
    private static final int I1 = 128;
    private static final byte[] SALT = hexStringToByteArray("e04fd020ea3a6910a2d808002b30309d");

    public static boolean validateString(String input) {
        return input.length() > ConstantsService.MIN_STRING_LEN && input.length() < ConstantsService.MAX_STRING_LEN;
    }

    public static boolean validateLogin(String input) {
        return input.length() > ConstantsService.MIN_LOGIN_LEN && input.length() < ConstantsService.MAX_LOGIN_LEN;
    }

    public static boolean validateEmail(String input) {
        return input.length() > ConstantsService.MIN_LOGIN_LEN && input.length() < ConstantsService.MAX_LOGIN_LEN && input.contains("@");
    }

    public static boolean validatePassword(String input) {
        return input.length() > ConstantsService.MIN_PASSWORD_LEN && input.length() < ConstantsService.MAX_STRING_LEN;
    }

    public static boolean validateImage(String input) {
        return input.length() > ConstantsService.MIN_IMAGE_LEN && input.length() < ConstantsService.MAX_STRING_LEN;
    }

    public static boolean validatePrice(double input) {
        return input > ConstantsService.MIN_PRICE && input < ConstantsService.MAX_PRICE;
    }

    public static boolean validateQuantity(int input) {
        return input > ConstantsService.MIN_QUANTITY && input < ConstantsService.MAX_QUANTITY;
    }

    public static String hashString(String input) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String output;

        KeySpec spec = new PBEKeySpec(input.toCharArray(), SALT, I, I1);
        SecretKeyFactory f = SecretKeyFactory.getInstance(HASH_METHOD);
        byte[] hash = f.generateSecret(spec).getEncoded();
        Base64.Encoder enc = Base64.getEncoder();

        output = enc.encodeToString(hash);
        return output;
    }

    public static boolean compareWithHashedString(String clean, String hashed) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeySpec spec = new PBEKeySpec(clean.toCharArray(), SALT, I, I1);
        SecretKeyFactory f = SecretKeyFactory.getInstance(HASH_METHOD);
        byte[] hash = f.generateSecret(spec).getEncoded();
        Base64.Encoder enc = Base64.getEncoder();

        clean = enc.encodeToString(hash);

        return clean.equals(hashed);
    }

    public static String getDate() {
        String dateString;

        SimpleDateFormat formatter= new SimpleDateFormat(ConstantsService.DATE_FORMAT);
        Date date = new Date(System.currentTimeMillis());

        dateString = formatter.format(date);

        return dateString;
    }
}
