package com.agh.fijak.api.models;

import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

public class ItemModel {

    public ItemModel() {}

    public ItemModel(
            @RequestParam(required = false) @Nullable int id,
            String name,
            double price,
            String image
    ) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int id;
    public String name;
    public double price;
    public String image;
}
