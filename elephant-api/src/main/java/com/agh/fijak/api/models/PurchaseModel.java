package com.agh.fijak.api.models;

import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RequestParam;

public class PurchaseModel {

    public PurchaseModel() {}

    public PurchaseModel(
            @RequestParam(required = false) @Nullable int id,
            int userId,
            int itemId,
            String date,
            int quantity
    ) {
        this.id = id;
        this.userId = userId;
        this.itemId = itemId;
        this.date = date;
        this.quantity = quantity;
    }

    public int id;
    public int userId;
    public int itemId;
    public String date;
    public int quantity;
}
